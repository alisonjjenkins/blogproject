#create a blogdb
CREATE DATABASE if not exists blogdb;
use blogdb;

#create BlogPost table within a blogdb
create TABLE if not exists BlogPost (
PostID TINYINT,
TitleOfPost VARCHAR(50),
MessageOfPost TEXT,
TimeStamp,
PRIMARY KEY (PostID)
);

#create BlogComment table within a blogdb
create TABLE if not exists BlogComment (
  CommentID TINYINT,
  WhoPosted TEXT,
  MessageOfComment VARCHAR(250),
  TimeStamp,
  PRIMARY KEY (CommentID)
);


 #create Users table within a blogdb
 create TABLE if not exists Users (
   Username varchar(12),
   UserEmail varchar(255),
   UserRealName varchar(255),
   UserGender BIT,
   UserAge TINYINT,
   Password VARCHAR(64),
#I assume that we will use sha-256 for PasswordSalt
   PasswordSalt CHAR(12),
   PRIMARY KEY (Username)
 );

  #create Categories table within a blogdb
  Create TABLE if not exists Categories (
    CategoryID TINYINT,
    CategoryName VARCHAR(12),
    PRIMARY KEY (CategoryID)
  );

 #create BlogPostCategory table within a blogdb
 Create TABLE if not exists BlogPostCategory (
PostID TINYINT
CategoryID TINYINT,
PRIMARY KEY (PostID, CategoryID)
);
