# Cool VIM shortcuts:

Append to the line that you are on: A
Insert at the beginning of the line: I
Insert before the character you currently have highlighted: i
Insert after the character you have highlighted: a
Jump forward from your cursor to a specified character: f<character>
Jump backward from your cursor to a specified character: F<character>
Jump to the next paragraph: }
Jump to the previous paragraph: {

Quicker save and quit: ZZ
Quicker quit without saving: ZQ

Show line numbers: :set number
Show relative numbers (number of lines from current line):  :set relnumber

