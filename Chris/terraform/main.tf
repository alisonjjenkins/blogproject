provider "aws" {
  region = "eu-west-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.nano"
  key_name = "alan-key"
  security_groups = ["${aws_security_group.allow_all.name}"]

  tags {
    Name = "Optimus"
  }
}

resource "aws_key_pair" "alan_laptop_key" {
  key_name  = "alan-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDKxkH3AL6Apfbirvkte64nl3rOUPBIb65Kfk111MfcXe9pHea0Nec+0tiOW9WFLORNteMNYdRqYqMrE60zoDSw8zTJcM7CiNum8oDzsXeBKNKZsbsvhkIEGGClX5douBitOYBncobJLcEfOCX+/rRbnv7Xzz2CTP0GyJJGfaU1+Rn0qLotcqQ8YEk/WrDXot1VdAOkFjgTLZsA36K/k6cQ8YVTbVnXbZysnBqzNIA6SL3aWlcBePaJ+M9WKyHUUYi+TPm+gcse1EmOl60oPl9lcjx3/i+YF1ViYCLxxYIrEf5Mm/WRtsEPsBzH5g98uaZ6zUmgS04u8sInp5y8GksRkrshd6pIjegt/XVnhNTT0yzynTqancefQSxt+cX7j3LeKnlV/Mnl1PK96Yh29odKkNYk4g9lImHJk+cupIgFmC5l/16HxfyHpzWVF3/Eu4QETedLMULkzxqknf9pSoUIR1QQ2ukrfHhxY6s9qoHlGtTk7u8C/AYc2cS74L/zong7somp3ZVlH+DzxDExiY9Z2CcZi1oiS3vKRn/psp20cWRuuDd7lN0fctmg7xoKktbAPYult8gu9DLMVyHFit+b7HvBmUcxvOpZl7fNNw9CA3ymk+Nne5SLUNoI8vT/NLqrSW69TgzOM2wrnYoCXTds/sw0Tr5CRZW+1I9FH5MywQ== alan@adfsfas.local"
}

resource "aws_security_group" "allow_all" {
  name          = "allow_all"
  description   = "allow all inbound traffic"

  tags {
    Name = "allow_all"
  }
}

resource "aws_security_group_rule" "allow_all" {
  type          = "ingress"
  from_port     = 0
  to_port       = 65535
  protocol      = "tcp"
  cidr_blocks   = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.allow_all.id}"
}

